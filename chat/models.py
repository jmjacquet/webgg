from django.db import models

# Create your models here.
class Comentario(models.Model):
	usuario = models.CharField(max_length=50)
	descripcion = models.TextField()
	fechayHora = models.DateTimeField(auto_now_add=True)
