# coding: utf-8
from django.conf.urls import *
from chat.views import PrincipalView,InicioView


urlpatterns = patterns('',

url(r'^$', PrincipalView.as_view()),
url(r'^inicio/$', InicioView.as_view()),
url(r'^comentar/$', 'chat.views.comentar'),

)



