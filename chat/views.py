from django.shortcuts import render,redirect
from django.views.generic import TemplateView
# Create your views here.
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse

from .forms import CommentForm
from .models import Comentario
import json

from datetime import date
hoy = date.today()

class PrincipalView(TemplateView):
    template_name = 'chat/chat.html'

    def post(self,request,*args,**kwargs):
        request.session['nombre'] = request.POST['nombre']
        return redirect('inicio/')

class InicioView(TemplateView):

    def get(self,request,*args,**kwargs):
        if request.session.get('nombre'):
            comentarios = Comentario.objects.filter(fechayHora__gt=hoy).order_by('-fechayHora')
            dic = {
                'nombre' : request.session['nombre'],
                'form' : CommentForm(),
                'comentarios' : comentarios,
                }
            return render(request,'chat/inicio.html',dic)
        else:
            return redirect('/chat/')

    def post(self,request,*args,**kwargs):
        Comentario.objects.create(
            usuario = request.session['nombre'],
            descripcion = request.POST['descripcion'],
            fechayHora = datetime.datetime.now()
            )
        return redirect('/chat/inicio')

@csrf_exempt
def comentar(request):
    Comentario.objects.create(
        usuario = request.POST.get('usuario'),
        descripcion = request.POST.get('descripcion')
        )
    response = {'usuario' : request.POST['usuario'],'descripcion': request.POST['descripcion'],}
    
    return HttpResponse(json.dumps(response),content_type='application/json')


