# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from django.contrib import admin
from webpage.views import *
from django.contrib.auth.decorators import login_required

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^login/$', 'webpage.views.login'),
    url(r'^logout/$', 'webpage.views.logout'),
    url(r'^administracion/$', login_required(GrupoguaView.as_view())),
    url(r'^$', PrincipalView.as_view()),
    url(r'^municipio/', include('webpage.urls')),
    
    #url(r'^tadese/consultapadrones.php?id=(?P<codigo>\d{1,3})','django.views.generic.simple.redirect_to',{'url': 'http://www.boletaweb.com.ar/municipios/consultapadrones.php?id=%(codigo)s'}),


)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += staticfiles_urlpatterns()
