import os
import site
import sys
sys.path.insert(0, '/home/grupogua/webapps/webgg/webGG')

site.addsitedir('/home/grupogua/webapps/webgg/lib/python2.7')

site.addsitedir('/home/grupogua/webapps/webgg')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "webGG.settingswf")


from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

