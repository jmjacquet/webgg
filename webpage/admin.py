# -*- coding: utf-8 -*-
from django.contrib import admin
from webpage.models import *
from datetime import datetime
from django.db.models import Sum,Count
from django.forms import SelectMultiple
from django.contrib.admin.views.main import ChangeList

def marcar_baja(modeladmin, request, queryset):
    queryset.update(baja=True,fecha_baja=datetime.now())

def volver_alta(modeladmin, request, queryset):
    queryset.update(baja=False,fecha_baja=None)

marcar_baja.short_description = "Marcar Baja del Sistema"
volver_alta.short_description = "Desmarcar Baja del Sistema"

class ContactoInline(admin.TabularInline):
    model = Municipio.contacto.through
    extra = 1
    verbose_name = 'Contactos'
    verbose_name_plural = 'Contactos'



class MunicipioAdmin(admin.ModelAdmin):

    formfield_overrides = { models.ManyToManyField: {'widget': SelectMultiple(attrs={'size':'8'})}, }

    def abonoPesos(self, obj):
        return '$ %.2f' % obj.abono
    abonoPesos.short_description = "Abono"

    def comisionPesos(self, obj):
        return '$ %.2f' % obj.comision
    comisionPesos.short_description = "Comisión"

    list_display = ('codigo','nombre', 'tipoMunicipio','comisionado','abonoPesos','comisionPesos','usaWeb','baja','fecha_baja','fecha_ult_logueo')
    list_display_links = ('codigo', 'nombre')
    list_filter = ('comisionado','tipoMunicipio','usaWeb','baja')
    search_fields = ['codigo','nombre']
    fieldsets = [
        ('Detalles del Municipio', {'fields': ['codigo', 'nombre', 'direccion', 'localidad','email','tipoMunicipio']}),
        ('Sistemas que Utilizan', {'fields': ['sistemas','usaWeb'], }),
        ('Datos Administrativos', {'fields': ['comisionado','comision','abono','tipoFacturero','meses_vencidos','deuda_aproximada','fecha_baja','baja','fecha_ult_logueo'],}),
        ('Ubicación', {'fields': ['position'], }),
        ('Detalles', {'fields': ['observaciones'], }),
        ]
    actions = [marcar_baja,volver_alta]
    inlines = [ContactoInline]

admin.site.register(Contacto)
admin.site.register(Sistema)
admin.site.register(Municipio, MunicipioAdmin)


