# -*- coding: utf-8 -*-
from tastypie.resources import ModelResource
from tastypie.constants import ALL
from models import Municipio

class MunicipioResource(ModelResource):
    class Meta:
        queryset = Municipio.objects.all()
        resource_name = 'Municipio'
        #filtering = { "title" : ALL }