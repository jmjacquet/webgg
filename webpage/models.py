# -*- encoding: utf-8 -*-
from django.db import models
from geoposition.fields import GeopositionField
from utilidades import *
from decimal import Decimal

class Contacto(models.Model):
    nombre = models.CharField(max_length=100)
    tipoContacto = models.CharField('Tipo de Contacto',max_length=1,choices=TIPO_CONTACTO,default=1)
    numero = models.PositiveIntegerField(max_length=45,null=True, blank=True,help_text='ej: 03424604953 (sin guiones)')
    email = models.EmailField(max_length=45, blank=True)

    class Meta:
        db_table = u'Contacto'
        verbose_name_plural = "Contactos"
        ordering = ['nombre','numero']

    def __unicode__(self):
        return u'%s - %s: %s' % (self.nombre,self.get_tipoContacto_display(), self.numero)

class Sistema(models.Model):
    nombre = models.CharField(max_length=100)

    class Meta:
        db_table = u'Sistema'
        verbose_name_plural = "Sistemas"
        ordering = ['nombre']

    def __unicode__(self):
        return u'%s ' % (self.nombre)

class Municipio(models.Model):
    codigo = models.PositiveIntegerField(null=True, blank=True)
    nombre = models.CharField(max_length=100)
    tipoMunicipio = models.CharField('Tipo de Entidad',max_length=1,choices=TIPO_MUNICIPIO,default=1)
    direccion = models.CharField(max_length=100, blank=True)
    localidad = models.CharField('Ciudad',max_length=100)
    email = models.EmailField(max_length=45, blank=True)
    contacto = models.ManyToManyField(Contacto,blank=True)
    position = GeopositionField('Ubicación')
    sistemas = models.ManyToManyField(Sistema)
    meses_vencidos = models.PositiveIntegerField(verbose_name = 'Meses Adeudados ',null=True, blank=True)
    deuda_aproximada = models.DecimalField(verbose_name = 'Deuda Aproximada ',max_digits=9, decimal_places=2,null=True, blank=True)
    abono = models.DecimalField(verbose_name = 'Abono ',max_digits=9, decimal_places=2,default=Decimal('0.00'))
    comision = models.DecimalField(verbose_name = 'Comisión ',max_digits=9, decimal_places=2,default=Decimal('0.00'))
    comisionado = models.BooleanField(default=True)
    tipoFacturero = models.CharField('Tipo de Facturero',max_length=1,choices=TIPO_FACTURERO,default=2)
    monto = models.DecimalField(verbose_name = 'Monto ',max_digits=9, decimal_places=2,default=Decimal('0.00'))
    baja = models.BooleanField(default=False)
    usaWeb = models.BooleanField(default=False)
    fecha_baja = models.DateField(verbose_name = 'Fecha de Baja',null=True, blank=True)
    fecha_ult_logueo = models.DateField(verbose_name = 'Fecha de Último Logueo',null=True, blank=True)
    observaciones = models.TextField(blank=True)

    class Meta:
        db_table = u'Municipio'
        verbose_name_plural = "Municipios"
        ordering = ['nombre','codigo']

    def __unicode__(self):
        return u'%s (%s de %s)' % (self.nombre,self.get_tipoMunicipio_display(), self.localidad)

    def get_nombre(self):
        return u'%s' % (self.nombre)

