# coding: utf-8
from django.conf.urls import *
from webpage.views import *
from django.contrib.auth.decorators import login_required

urlpatterns = patterns('webpage.municipio',

    url(r'^$', PrincipalView.as_view()),
    url(r'^(?P<codigo>\d{1,5})/$', datosMunicipios,name='municipio_baja'),
    url(r'^(?P<codigo>\d{1,5})/fb/$', fbajaMunic,name='municipio_fbaja'),
    url(r'^(?P<codigo>\d{1,5})/fl/$', flogueoMunic,name='municipio_flogueo'),
    url(r'^(?P<codigo>\d{1,5})/fb/(?P<day>\d{1,2})/(?P<month>\d{1,2})/(?P<year>\d{4})/$', fbajaSubir,name='municipio_fbajaSubir'),
    url(r'^(?P<codigo>\d{1,5})/fl/(?P<day>\d{1,2})/(?P<month>\d{1,2})/(?P<year>\d{4})/$', flogueoSubir,name='municipio_flogueoSubir'),

)



