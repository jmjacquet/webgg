# -*- coding: utf-8 *-*

TIPO_DOC = (
    ( 'DNI','DNI'),
    ( 'LE','LE'),
    ( 'LC','LC'),
    ( 'Otro','Otro'),
    )

TIPO_MUNICIPIO = (
    ('1', 'Comuna'),
    ('2', 'Municipalidad'),
    ('3', 'Ente'),
    ('4', 'Privado'),
    )

TIPO_SISTEMA_GG = (
    ('1', 'Tadese'),
    ('2', 'Comunal Contable'),
    ('3', 'Administrativo'),
    ('4', 'Sueldos'),
    ('5', 'Expedientes'),
    ('6', 'Sistema WEB'),
    ('7', 'Callcenter'),
    )

TIPO_CONTACTO = (
    ('1', 'Teléfono'),
    ('2', 'Celular'),
    ('3', 'Otro'),
    )

TIPO_FACTURERO = (
    ('1', 'GrupoGuadalupe'),
    ('2', 'Marisel'),
    ('3', 'Recibos X'),
    ('3', 'LHQ'),
    )

TIPO_ESTADO_CIVIL = (
    ('1', 'Soltero'),
    ('2', 'Casado'),
    ('3', 'Separado'),
    ('4', 'Otro'),
    )




