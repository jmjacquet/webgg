# -*- coding: utf-8 -*-
from django.contrib.auth import authenticate, login as django_login, logout as django_logout
from django.shortcuts import *
from webpage.models import *
from django.contrib.auth.decorators import login_required
from webGG.settings import *
from django.views.generic import TemplateView
import datetime


def login(request):
    error = None
    if request.method == 'POST':
        user = authenticate(username=request.POST['username'], password=request.POST['password'])
        if user is not None:
          if user.is_active:
            django_login(request, user)
            # success
            return HttpResponseRedirect(LOGIN_REDIRECT_URL)
          else:
          ## invalid login
           error = "Usuario/Password incorrectos."
        else:
          ## invalid login
           error = "Usuario/Password incorrectos."
          #return direct_to_template(request, 'invalid_login.html')
    return render_to_response('base.html', {'root_url': ROOT_URL,'error': error},context_instance=RequestContext(request))

@login_required
def logout(request):
    request.session.clear()
    django_logout(request)
    return HttpResponseRedirect(ROOT_URL)


class PrincipalView(TemplateView):
    template_name = 'base.html'
    context_object_name = 'muni'

    def get_context_data(self, **kwargs):
        context = super(PrincipalView, self).get_context_data(**kwargs)
        context['muni'] = Municipio.objects.exclude(position__isnull=True).filter(baja=False)
        return context


class GrupoguaView(TemplateView):
    template_name = 'administracion/listado_munis.html'
    context_object_name = 'muni'

    def get_context_data(self, **kwargs):
        context = super(GrupoguaView, self).get_context_data(**kwargs)
        context['muni'] = Municipio.objects.exclude(position__isnull=True).filter(baja=False)
        context['object_list'] = Municipio.objects.exclude(position__isnull=True).filter(baja=False)
        return context

def datosMunicipios(request, codigo):
    try:
        muni = Municipio.objects.get(codigo=codigo)
    except Municipio.DoesNotExist:
        raise Http404
    return render_to_response('municipio_baja.html',{'muni':muni},context_instance=RequestContext(request))

def fbajaMunic(request, codigo):
    try:
        muni = Municipio.objects.get(codigo=codigo)
    except Municipio.DoesNotExist:
        raise Http404
    return render_to_response('municipio_fbaja.html',{'muni':muni},context_instance=RequestContext(request))

def flogueoMunic(request, codigo):
    try:
        muni = Municipio.objects.get(codigo=codigo)
    except Municipio.DoesNotExist:
        raise Http404
    return render_to_response('municipio_flogueo.html',{'muni':muni},context_instance=RequestContext(request))

def fbajaSubir(request, codigo,day,month,year):
    try:
        muni = Municipio.objects.get(codigo=codigo)
        muni.fecha_baja = datetime.date(int(year),int(month),int(day))
        muni.save()
    except Municipio.DoesNotExist:
        raise Http404
    return HttpResponse("True")


def flogueoSubir(request, codigo,day,month,year):
    try:
        muni = Municipio.objects.get(codigo=codigo)
        muni.fecha_ult_logueo = datetime.date(int(year),int(month),int(day))
        muni.save()
    except Municipio.DoesNotExist:
        raise Http404
    return HttpResponse("True")